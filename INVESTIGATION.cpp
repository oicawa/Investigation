#include "INVESTIGATION.h"
#include <stdio.h>
#include <tchar.h>
#include <winbase.h>

#include <time.h>
#include <sys/stat.h>

#include <dbghelp.h>
#pragma comment(lib, "dbghelp.lib")

#include "Shlwapi.h"
#pragma comment(lib, "Shlwapi.lib")

#include <psapi.h>
#pragma comment ( lib, "psapi.lib" )


// --------------------------------------------------
// Global variables (only in INVESTIGATION.cpp file.)
// --------------------------------------------------
static Logger loggers[5] = { NULL, NULL, NULL, NULL, NULL };
static TCHAR cleanupFilter[256];
static TCHAR targetProcessNames[2014];
static DWORD targetProcessIds[32];

// --------------------------------------------------
// Private function declarations (only in INVESTIGATION.cpp file.)
// --------------------------------------------------
static void CleanupSymbols();
static void InitializeSymbols(HANDLE& process, LPCTSTR filter, DWORD& stackSize);
static bool GetProcessName(LPTSTR pBuffer, size_t countOfBuffer);
static void ManageProcess();
static int GetTargetProcessIdIndex(DWORD currentProcessId);
static bool IsTargetProcess();
static void OutputDebugStringToDebugView(LPCTSTR filter, LPCTSTR log);
static void OutputDebugStringToFile(LPCTSTR filter, LPCTSTR log);

// --------------------------------------------------
// Public function definitions
// --------------------------------------------------
void Investigation_ManageLogger(Logger logger, bool add)
{
	// Search Indexes
	int emptyIndex = -1;	// To add logger
	int existIndex = -1;	// To remove logger
	DEBUG_LOG_INV(_T("logger=0x%p, add=%d"), logger, add);
	for (size_t i = 0; i < _countof(loggers); i++)
	{
		DEBUG_LOG_INV(_T("loggers[%Iu]=0x%p"), i, loggers[i]);
		if (loggers[i] == NULL && emptyIndex < 0)
		{
			emptyIndex = (int)i;
		}

		if (loggers[i] == logger && existIndex < 0)
		{
			existIndex = (int)i;
		}
	}

	if (add)
	{
		// Add logger
		if (0 <= existIndex)
		{
			// Already exists
			DEBUG_LOG_INV(_T("Already exists at loggers[%d](=0x%p)"), existIndex, loggers[existIndex]);
			return;
		}
		if (emptyIndex < 0)
		{
			// No empty index
			DEBUG_LOG_INV(_T("No empty indexes"));
			return;
		}
		// Regist logger
		loggers[emptyIndex] = logger;
		DEBUG_LOG_INV(_T("SET loggers[%d] = logger(=0x%p)"), emptyIndex, logger);
	}
	else
	{
		// Remove logger
		if (existIndex < 0)
		{
			// Not exists
			DEBUG_LOG_INV(_T("Not exists"));
			return;
		}
		loggers[existIndex] = nullptr;
		DEBUG_LOG_INV(_T("DEL loggers[%d] = null"), existIndex);
	}
}

bool Investigation_IsTarget(LPCTSTR filter)
{
	if (filter == nullptr)
	{
		return false;
	}
	if (_tcslen(filter) == 0)
	{
		return false;
	}

	static DWORD lastTickCount = 0;
	static time_t lastUpdatedIniFile = 0;
	DWORD lastTicCountBackup = lastTickCount;
	if (Investigation_WatchIniFileLastUpdated(lastTickCount, lastUpdatedIniFile, filter))
	{
		// DbgView Logger
		bool addDbgView = GetPrivateProfileInt(_T("Output"), _T("DebugView"), 1, _T(INVESTIGATION_INI_FILE_PATH)) == 0 ? false : true;
		Investigation_ManageLogger(OutputDebugStringToDebugView, addDbgView);

		// File Logger
		TCHAR logFilePath[MAX_PATH];
		ZeroMemory(logFilePath, sizeof(logFilePath));
		DWORD length = GetPrivateProfileString(_T("Output"), _T("File"), _T(""), logFilePath, sizeof(logFilePath), _T(INVESTIGATION_INI_FILE_PATH));
		bool addFile = (0 < length) ? true : false;
		Investigation_ManageLogger(OutputDebugStringToFile, addFile);

		// Process
		ManageProcess();
	}
	DEBUG_LOG_INV(_T("lastTickCount = %lu -> %lu"), lastTicCountBackup, lastTickCount);

	if (IsTargetProcess() == false)
	{
		return false;
	}

	return true;
}

void Investigation_MakeLog(LPCTSTR filter, LPCTSTR file, int line, LPCTSTR function, LPTSTR log, size_t countOfLog, LPCTSTR format, ...)
{
	// Position (file, line, function)
	DWORD threadId = GetCurrentThreadId();
	TCHAR position[1024];
	ZeroMemory(position, sizeof(position));
	_stprintf_s(position, _countof(position), _T("(TID:%lu)[%s:(%d)/%s] "), threadId, PathFindFileName(file), line, function);

	// Message
	TCHAR message[1024];
	ZeroMemory(message, sizeof(message));
	va_list args;
	va_start(args, format);
	int result = _vstprintf_s(message, _countof(message), format, args);
	va_end(args);

	// Make Log
	ZeroMemory(log, sizeof(log));
	_tcscat_s(log, countOfLog, filter);
	_tcscat_s(log, countOfLog, position);
	_tcscat_s(log, countOfLog, message);
}

void Investigation_DebugLog(LPCTSTR filter, LPCTSTR log)
{
	// Write Log
	for (size_t i = 0; i < _countof(loggers); i++)
	{
		if (loggers[i] == NULL)
			break;
		loggers[i](filter, log);
	}
}

#ifdef x86
#define StackType PVOID
#define StackUnitSize DWORD
#define SymbolDisplacement DWORD
#define LineDisplacement DWORD
#else
#define StackType DWORD64
#define StackUnitSize DWORD64
#define SymbolDisplacement DWORD64
#define LineDisplacement DWORD
#endif

void Investigation_DebugLogStack(LPCTSTR filter, LPCTSTR filterEx, LPCTSTR file, int line, LPCTSTR function)
{
	static bool isInitialized = false;
	static DWORD stackSize = 20;

	HANDLE process = GetCurrentProcess();

	// Initialize
	if (isInitialized == false)
	{
		isInitialized = true;
		InitializeSymbols(process, filter, stackSize);
	}

	// Stack
	StackType* pStack = (StackType*)malloc(sizeof(StackType) * stackSize);
	if (pStack == nullptr)
	{
		return;
	}
	unsigned short frames = CaptureStackBackTrace(0, stackSize, (PVOID*)pStack, NULL);

	// Output each stack information
	unsigned short lastFrameIndex = (frames == 0) ? 0 : (frames - 1);
	for (unsigned int i = 0; i < lastFrameIndex; i++)
	{
		// Symbol Name
		IMAGEHLP_SYMBOL * pImageSymbol;
		char buffer[sizeof(IMAGEHLP_SYMBOL) + MAX_PATH] = { 0 };
		pImageSymbol = (IMAGEHLP_SYMBOL*)buffer;
		pImageSymbol->SizeOfStruct = sizeof(IMAGEHLP_SYMBOL);
		pImageSymbol->MaxNameLength = MAX_PATH;
		SymbolDisplacement symbolDisplacement = 0;
		SymGetSymFromAddr(process, (StackUnitSize)pStack[i], &symbolDisplacement, pImageSymbol);
		TCHAR symbolName[256];
		ZeroMemory(symbolName, sizeof(symbolName));
		MultiByteToWideChar(CP_OEMCP, MB_PRECOMPOSED, pImageSymbol->Name, (int)strlen(pImageSymbol->Name), symbolName, (int)(sizeof symbolName) / 2);
		if (_tcscmp(symbolName, _T(__FUNCTION__)) == 0)
		{
			continue;
		}

		// Module Name
		IMAGEHLP_MODULE imageModule = { sizeof(IMAGEHLP_MODULE) };
		SymGetModuleInfo(process, (StackUnitSize)pStack[i], &imageModule);
		TCHAR moduleName[256];
		ZeroMemory(moduleName, sizeof(moduleName));
		MultiByteToWideChar(CP_OEMCP, MB_PRECOMPOSED, imageModule.ModuleName, (int)strlen(imageModule.ModuleName), moduleName, (int)(sizeof moduleName) / 2);

		// Line Number
		LineDisplacement lineDisplacement = 0;
		IMAGEHLP_LINE imageLine = { sizeof(IMAGEHLP_LINE) };
		SymGetLineFromAddr(process, (StackUnitSize)pStack[i], &lineDisplacement, &imageLine);

		// Write
		TCHAR log[1024];
		ZeroMemory(log, sizeof(log));
		Investigation_MakeLog(filterEx ? filterEx : filter , file, line, function, log, _countof(log), _T("   %i: [%s] %s - 0x%0X(Line:%d)"), frames - i - 1, moduleName, symbolName, pImageSymbol->Address, imageLine.LineNumber);
		Investigation_DebugLog(filter, log);
	}
	free(pStack);
}

bool Investigation_MakeBitsString(unsigned long long unsignedValue, size_t byteSizeOfValue, LPTSTR pBuffer, size_t countOfBuffer)
{
	if (pBuffer == nullptr)
	{
		return false;
	}
	const size_t BITCOUNT_OF_BYTE = 8;
	ZeroMemory(pBuffer, countOfBuffer * sizeof(TCHAR));
	size_t allBitsCount = BITCOUNT_OF_BYTE * byteSizeOfValue;
	if (countOfBuffer < allBitsCount)
	{
		return false;
	}

	unsigned long long tmpUnsignedValue = unsignedValue;
	for (size_t i = 0; i < allBitsCount; i++)
	{
		size_t separatorCount = (allBitsCount - i -1) / BITCOUNT_OF_BYTE;
		size_t index = (allBitsCount - 1) - i + separatorCount;
		bool requiredSeparator = (((i + 1) % BITCOUNT_OF_BYTE) == 0  && 1 <= index) ? true : false;
		if (requiredSeparator)
		{
			pBuffer[index - 1] = _T('-');
		}

		pBuffer[index] = (tmpUnsignedValue % 2 == 0) ? _T('0') : _T('1');
		tmpUnsignedValue = tmpUnsignedValue / 2;
	}
	return true;
}

bool Investigation_WatchIniFileLastUpdated(DWORD& lastTickCount, time_t& lastUpdatedIniFile, LPCTSTR filter)
{
#ifdef DEBUG_LOG_INI_WATCH_MSEC
	// Log buffer
	TCHAR log[1024] = { 0 };
	ZeroMemory(log, sizeof(log));

	// Check Watch Interval by TickCount
	DWORD currentTickCount = GetTickCount();
	if (currentTickCount - lastTickCount < DEBUG_LOG_INI_WATCH_MSEC)
	{
		DEBUG_LOG_INV(_T("IniFile(%s) currentTickCount(=%lu) - lastTickCount(=%lu) = %lu < DEBUG_LOG_INI_WATCH_MSEC(=%lu)"),
			_T(INVESTIGATION_INI_FILE_PATH), currentTickCount, lastTickCount, currentTickCount - lastTickCount, DEBUG_LOG_INI_WATCH_MSEC);
		return false;
	}
	// Update Tick Count
	lastTickCount = currentTickCount;
	DEBUG_LOG_INV(_T("IniFile(%s) Update lastTickCount(=%lu)"), _T(INVESTIGATION_INI_FILE_PATH), lastTickCount);

	// Get Ini file's last updated time.
	struct stat st = { 0 };
	stat(INVESTIGATION_INI_FILE_PATH, &st);

	// Compare Init file's last updated time between getting time and on memory time.
	if (st.st_mtime <= lastUpdatedIniFile)
	{
		DEBUG_LOG_INV(_T("IniFile(%s) Not Updated"), _T(INVESTIGATION_INI_FILE_PATH));
		return false;
	}
	// Update last updated time on memory.
	lastUpdatedIniFile = st.st_mtime;

	// Logging
	struct tm * pLocalTime = localtime(&st.st_mtime);
	TCHAR time[128] = { 0 };
	ZeroMemory(time, sizeof(time));
	_stprintf_s(time, _countof(time), _T("%s"), pLocalTime == nullptr ? _T("(null)") : _tasctime(pLocalTime));
	DEBUG_LOG_INV(_T("IniFile(%s) Updated at [%s]"), _T(INVESTIGATION_INI_FILE_PATH), time);

	return true;

#else
	// If 'INVESTIGATION_INI_FILE_WATCH_INTERVAL_MSEC' was not defined,
	// Investigation module read only one time at first time.
	if (lastTickCount == 0)
	{
		lastTickCount = GetTickCount();
		return true;
	}
	else
	{
		return false;
	}
#endif
}

bool Investigation_Inject(int remoteProcessId, LPCTSTR dllFullPath)
{
	//HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	//void *datamemory = VirtualAllocEx(hProcess, NULL, sizeof(dllpath), MEM_COMMIT, PAGE_READWRITE);
	//WriteProcessMemory(hProcess, datamemory, (void *)dllpath, sizeof(dllpath), NULL);

	char multiCharDllFullPath[MAX_PATH];
	BOOL result = FALSE;
	WideCharToMultiByte(CP_OEMCP, WC_SEPCHARS, dllFullPath, -1, multiCharDllFullPath, sizeof(multiCharDllFullPath), nullptr, &result);
	if (result == TRUE)
	{
		DEBUG_LOG_INV(_T("'dllFullPath' variable has illegal wide charactor(s) to convert multi byte character(s)."));
		return false;
	}

	// Open the remote process handle.
	HANDLE hRemoteProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, remoteProcessId);
	DEBUG_LOG_INV(_T("hRemoteProcess=0x%x"), hRemoteProcess);

	// Get the remote process's data memory.
	void *dataMemory = VirtualAllocEx(hRemoteProcess, NULL, sizeof(multiCharDllFullPath), MEM_COMMIT, PAGE_READWRITE);
	DEBUG_LOG_INV(_T("dataMemory=0x%p"), dataMemory);

	// Write a new library path into the gotton remote process data memory.
	result = WriteProcessMemory(hRemoteProcess, dataMemory, (void *)multiCharDllFullPath, sizeof(multiCharDllFullPath), NULL);
	DEBUG_LOG_INV(_T("result=%d"), result);

	// Get a handle of kernel32.dll
	HMODULE kernel32 = GetModuleHandle(_T("kernel32"));
	DEBUG_LOG_INV(_T("kernel32=0x%x"), kernel32);

	// Get an address of 'LoadLibraryA' function from the above handle.
	FARPROC loadLibrary = GetProcAddress(kernel32, "LoadLibraryA");
	//FARPROC loadLibrary = GetProcAddress(kernel32, "LoadLibraryW");
	DEBUG_LOG_INV(_T("loadLibrary=0x%p"), loadLibrary);

	// Create a remote thread & run LoadLibraryA function with the specified new library path in dataMemory.
	HANDLE hRemoteThread = CreateRemoteThread(hRemoteProcess, NULL, 0, (LPTHREAD_START_ROUTINE)loadLibrary, dataMemory, 0, NULL);
	DEBUG_LOG_INV(_T("hRemoteThread=0x%x"), hRemoteThread);
	if (!hRemoteThread)
	{
		/* 32 bit (WOW64) -> 64 bit (Native) won't work */
		TCHAR errmsg[512];
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(), 0, errmsg, sizeof(errmsg), NULL);
		DEBUG_LOG_INV(_T("errmsg=%hs"), errmsg);
		goto END;
	}

	// Wait for the end of LoadLibraryA function call.
	WaitForSingleObject(hRemoteThread, INFINITE);
	DEBUG_LOG_INV(_T("Compeleted WaitForSingleObject(...)"));

END:
	DEBUG_LOG_INV(_T("Close 'hRemoteThread'"));
	CloseHandle(hRemoteThread);
	DEBUG_LOG_INV(_T("Free memory of Close 'dataMemory'"));
	VirtualFreeEx(hRemoteProcess, dataMemory, sizeof(multiCharDllFullPath), MEM_RELEASE);

	return true;
}

// --------------------------------------------------
// Private function definitions (only in INVESTIGATION.cpp file.)
// --------------------------------------------------
static void CleanupSymbols()
{
	SymCleanup(GetCurrentProcess());

	TCHAR log[1024];
	ZeroMemory(log, sizeof(log));
	Investigation_MakeLog(cleanupFilter, _T(__FILE__), __LINE__, _T(__FUNCTION__), log, _countof(log), _T("Symbols cleanup completed."));
	Investigation_DebugLog(cleanupFilter, log);
}

static void InitializeSymbols(HANDLE& process, LPCTSTR filter, DWORD& stackSize)
{
	// Read Stack Size
	stackSize = GetPrivateProfileInt(_T("StackTrace"), _T("Size"), stackSize, _T(INVESTIGATION_INI_FILE_PATH));

	// Read Symbol Path
	char symbolPathA[1024];
	ZeroMemory(symbolPathA, sizeof(symbolPathA));
	GetPrivateProfileStringA("StackTrace", "SymbolPath", "C:\\Symbols", symbolPathA, sizeof(symbolPathA), INVESTIGATION_INI_FILE_PATH);

	// Initialize(Load) Symbol files
	SymInitialize(process, symbolPathA, TRUE);
	DWORD64 result = SymLoadModule(process, NULL, NULL, NULL, 0, 0);

	// Register a function which cleans up symbols when current process exit.
	_tcscpy_s(cleanupFilter, filter);
	atexit(CleanupSymbols);

	// Write Log
	TCHAR log[1024];
	ZeroMemory(log, sizeof(log));
	Investigation_MakeLog(filter, _T(__FILE__), __LINE__, _T(__FUNCTION__), log, _countof(log), _T("InitializeSymbols(...) completed."));
	Investigation_DebugLog(filter, log);
}

bool GetProcessName(LPTSTR pBuffer, size_t countOfBuffer)
{
	ZeroMemory(pBuffer, countOfBuffer * sizeof(TCHAR));

	DWORD dwProcessId = ::GetCurrentProcessId();
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwProcessId);
	if (hProcess == nullptr)
	{
		return false;
	}

	::GetModuleBaseName(hProcess, NULL, pBuffer, (DWORD)countOfBuffer);

	::CloseHandle(hProcess);

	return true;
}

void ManageProcess()
{
	ZeroMemory(targetProcessNames, sizeof(targetProcessNames));
	GetPrivateProfileString(_T("Processes"), _T("Target"), _T(""), targetProcessNames, sizeof(targetProcessNames), _T(INVESTIGATION_INI_FILE_PATH));
	if (_tcslen(targetProcessNames) == 0)
	{
		DEBUG_LOG_INV(L"Target process names are not Specified.");
		return;
	}
	_tcsupr_s(targetProcessNames);
	DEBUG_LOG_INV(L"Target process names = [%s]", targetProcessNames);

	TCHAR processName[BUFSIZ];
	ZeroMemory(processName, sizeof(processName));
	GetProcessName(processName, _countof(processName));
	_tcsupr_s(processName);
	DEBUG_LOG_INV(L"Current process name = [%s]", processName);
	if (_tcsstr(targetProcessNames, processName) == nullptr)
	{
		// Not register
		DEBUG_LOG_INV(L"Not HIT");
		return;
	}

	// Check existing process ID
	DWORD currentProcessId = GetCurrentProcessId();
	DEBUG_LOG_INV(L"Current Process ID = %lu", currentProcessId);
	if (0 <= GetTargetProcessIdIndex(currentProcessId))
	{
		// Already registered
		DEBUG_LOG_INV(L"Already registered");
		return;
	}

	// Register the current process ID
	bool alreadyRegistered = false;
	for (size_t i = 0; i < _countof(targetProcessIds); i++)
	{
		if ((targetProcessIds[i] == 0) && (alreadyRegistered == false))
		{
			targetProcessIds[i] = currentProcessId;
			DEBUG_LOG_INV(L"targetProcessIds[%Iu]=%lu <- !!NEW!!", i, targetProcessIds[i]);
			alreadyRegistered = true;
		}
		else
		{
			DEBUG_LOG_INV(L"targetProcessIds[%Iu]=%lu", i, targetProcessIds[i]);
		}
	}
}

int GetTargetProcessIdIndex(DWORD currentProcessId)
{
	for (size_t i = 0; i < _countof(targetProcessIds); i++)
	{
		if (targetProcessIds[i] == currentProcessId)
		{
			DEBUG_LOG_INV(L"Registered index = %d", i);
			return (int)i;
		}
	}

	DEBUG_LOG_INV(L"Not exist");
	return -1;
}

bool IsTargetProcess()
{
	// If Not specified, all processes are target.
	if (_tcslen(targetProcessNames) == 0)
	{
		DEBUG_LOG_INV(L"All processes are target.");
		return true;
	}


	if (0 <= GetTargetProcessIdIndex(GetCurrentProcessId()))
	{
		DEBUG_LOG_INV(L"Current process is a target.");
		return true;
	}

	DEBUG_LOG_INV(L"Current process is NOT a target.");
	return false;
}

// --------------------------------------------------
// Out put debug string functions
// --------------------------------------------------
static void OutputDebugStringToDebugView(LPCTSTR filter, LPCTSTR log)
{
	OutputDebugString(log);
}

static void OutputDebugStringToFile(LPCTSTR filter, LPCTSTR log)
{
	static TCHAR logFilePath[MAX_PATH] = { 0 };
	static TCHAR mutexName[BUFSIZ];
	if (_tcslen(logFilePath) == 0)
	{
		// Get log file path
		TCHAR tmpLogFilePath[MAX_PATH] = { 0 };
		ZeroMemory(tmpLogFilePath, sizeof(tmpLogFilePath));
		DWORD length = GetPrivateProfileString(_T("Output"), _T("File"), _T(""), tmpLogFilePath, sizeof(tmpLogFilePath), _T(INVESTIGATION_INI_FILE_PATH));
		if (_tcslen(tmpLogFilePath) == 0)
		{
			return;
		}
		ZeroMemory(logFilePath, sizeof(logFilePath));
		_stprintf_s(logFilePath, _T("%s"), tmpLogFilePath);
		ZeroMemory(mutexName, sizeof(mutexName));
		_stprintf_s(mutexName, _T("INVESTIGATION-%s"), tmpLogFilePath);
	}

	// Create Mutex object & regist close mutex procedure at end of process.
	HANDLE hMutex = CreateMutex(NULL, FALSE, mutexName);
	WaitForSingleObject(hMutex, INFINITE);

	// Write log message.
	//FILE* pFile = _tfopen_s((logFilePath, _T("a"));
	FILE* pFile = nullptr;
	errno_t err = _tfopen_s(&pFile, logFilePath, _T("a"));
	if (pFile != nullptr)
	{
		_fputts(log, pFile);
		_fputts(_T("\n"), pFile);
		_fflush_nolock(pFile);
		fclose(pFile);
	}

	// Release & Close Mutex handle
	ReleaseMutex(hMutex);
	CloseHandle(hMutex);
}

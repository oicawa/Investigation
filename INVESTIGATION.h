#ifndef _INVESTIGATION_
#define _INVESTIGATION_

#include <windows.h>
#include <stdlib.h>

#define INVESTIGATION_INI_FILE_PATH "C:\\Investigation.ini"
//#define INVESTIGATION_INI_FILE_WATCH_INTERVAL_MSEC	10000
//#define DEBUG_LOG_INV_FILTER _T("DEBUG_LOG_INV")

typedef void(*Logger)(LPCTSTR filter, LPCTSTR message);

// --------------------------------------------------
// Public function declarations
// --------------------------------------------------
void Investigation_ManageLogger(Logger logger, bool add);
bool Investigation_IsTarget(LPCTSTR filter);
void Investigation_MakeLog(LPCTSTR filter, LPCTSTR file, int line, LPCTSTR function, LPTSTR log, size_t countOfLog, LPCTSTR format, ...);
void Investigation_DebugLog(LPCTSTR filter, LPCTSTR log);
void Investigation_DebugLogStack(LPCTSTR filter, LPCTSTR filterEx, LPCTSTR file, int line, LPCTSTR function);
bool Investigation_MakeBitsString(unsigned long long unsignedValue, size_t byteSizeOfValue, LPTSTR pBuffer, size_t countOfBuffer);
bool Investigation_WatchIniFileLastUpdated(DWORD& lastTickCount, time_t& lastUpdatedIniFile, LPCTSTR filter);
bool Investigation_Inject(int processId, LPCTSTR dllFullPath);

// --------------------------------------------------
// Public Macros (Normally you should use these macros.)
// --------------------------------------------------
#define DEBUG_LOG(filter, format, ...) \
	do { \
		if (Investigation_IsTarget(filter) == false) break; \
		TCHAR _____log[1024];\
		ZeroMemory(_____log, sizeof(_____log));\
		Investigation_MakeLog(filter, _T(__FILE__), __LINE__, _T(__FUNCTION__), _____log, _countof(_____log), format, __VA_ARGS__);\
		Investigation_DebugLog(filter, _____log);\
	} while (0);

#define DEBUG_LOG_STACK(filter, format, ...) \
	do { \
		if (Investigation_IsTarget(filter) == false) break; \
		TCHAR _____formatEx[256];\
		ZeroMemory(_____formatEx, sizeof(_____formatEx));\
		_tcscat_s(_____formatEx, _T(">> CallStack: "));\
		_tcscat_s(_____formatEx, format);\
		TCHAR _____log[1024];\
		ZeroMemory(_____log, sizeof(_____log));\
		Investigation_MakeLog(filter, _T(__FILE__), __LINE__, _T(__FUNCTION__), _____log, _countof(_____log), _____formatEx, __VA_ARGS__);\
		Investigation_DebugLog(filter, _____log);\
		Investigation_DebugLogStack(filter, nullptr, _T(__FILE__), __LINE__, _T(__FUNCTION__));\
		ZeroMemory(_____log, sizeof(_____log));\
		Investigation_MakeLog(filter, _T(__FILE__), __LINE__, _T(__FUNCTION__), _____log, _countof(_____log), _T("<< CallStack"));\
		Investigation_DebugLog(filter, _____log);\
	} while (0);


#ifdef DEBUG_LOG_INV_FILTER
#define DEBUG_LOG_INV(format, ...) \
	do {\
		TCHAR _____log[1024];\
		ZeroMemory(_____log, sizeof(_____log));\
		Investigation_MakeLog(_T("INVESTIGATION-Debug"), _T(__FILE__), __LINE__, _T(__FUNCTION__), _____log, _countof(_____log), format, __VA_ARGS__);\
		OutputDebugString(_____log);\
	} while (0);
#else
#define DEBUG_LOG_INV(format, ...) ;
#endif

#define DEBUG_LOG_DECLARE_FILTER(filter, expression) \
	LPCTSTR _____dummy_##filter=nullptr,##filter = (expression) ? _T(#filter) : nullptr;

#define MAKE_BIT_STRING(unsignedValue, buffer) \
	TCHAR _____dummy_##buffer,##buffer[BUFSIZ];\
	_____dummy_##buffer = _T('\0');\
	ZeroMemory(##buffer, sizeof(##buffer));\
	Investigation_MakeBitsString(##unsignedValue, sizeof(##unsignedValue), ##buffer, _countof(##buffer));

#define DEBUG_PARAM_INT(key) \
	(GetPrivateProfileInt(_T("Parameters"), key, 0, _T(INVESTIGATION_INI_FILE_PATH)))

#endif _INVESTIGATION_
